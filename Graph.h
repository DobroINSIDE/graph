#ifndef GRAPH_H
#define GRAPH_H

#include "iostream"
#include "string"
#include "algorithm"
#include "map"
#include "sstream"
#include "fstream"
#include "vector"
#include "tuple"
#include "climits"
#include "utility"
#include "dsu.h"

using namespace std;

class Graph
{
    public:
        Graph(int);
        void readGraph(string);
        void writeGraph(string);
        int changeEdge(int, int, int);
        void addEdge(int, int, int);
        void removeEdge(int, int);
        void transformToAdjMatrix();
        void transformToListOfEdges();
        void transformToAdjList();

        Graph getSpaingTreeBoruvka();
        Graph getSpaingTreeKruskal();
        Graph getSpaingTreePrima();

        int checkEuler(bool &circleExist);
        vector<int> getEuleranTourFleri();
	      vector<int> getEuleranTourEffective();
	      bool empty();
    	  bool fleury(vector<int>);

	      int checkBipart(vector<char> &marks);
    	  vector<pair<int,int> > getMaximumMatchingBipart();
    	  bool kun(int v);

    private:
        char type;
        unsigned int nodeCount;
        bool weighted, oriented;

        vector<vector<int>> adjMatrix;
        vector<vector<pair<int,int>>> adjList;
        vector<vector<tuple<int,int,int>>> weightedAdjList;
        vector<pair<int,int>> edgeList;
        vector<tuple<int,int,int>> weightedEdgeList;
};
