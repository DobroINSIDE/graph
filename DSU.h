#ifndef DSU_H
#define DSU_H

#include <cstdio>

class DSU
{
    public:
        int p[1000], rank[1000];
        DSU(int);
        int find(int);
        bool unite(int, int);
};

#endif
