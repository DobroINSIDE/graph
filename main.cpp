#include <Graph.h>

int main()
{
  Graph graph;
  graph.readGraph("input.txt");
  graph.transformToAdjList();
  graph.transformToListOfEdges();
  graph.transformToAdjMatrix();
  graph.writeGraph("output.txt");


  /*//4 лаба
  Graph graph = Graph(0);
  graph.readGraph("bipart.txt");

  vector<char> marks;
  int i = graph.checkBipart(marks);
  if (i != -1)
  {
    cout << "True " << i << endl;
  } else {
    cout << "False " << endl;
  }

  vector<pair<int,int>> x = graph.getMaximumMatchingBipart();

  for (int i = 0; i < x.size(); i++)
  {
    cout << "(" << x[i].first << ", " << x[i].second << ")" << endl;
  }

  cin.get();*/

  /*//3 лаба
  Graph graph = Graph(0);
  graph.readGraph("input.txt");

  vector<int> circuit1 = graph.getEuleranTourFleri();
  vector<int> circuit2 = graph.getEuleranTourEffective();

  cout << "Eulerian tour with Fleury's:" << endl;
  for (int i = 0; i <  circuit1.size(); i++)
  {
  cout << circuit1[i] + 1 << " ";
  }
  cout << "\n\n";
  cout << "Eulerian tour with effective algorithm:" << endl;
  for (int i = 0; i < circuit2.size(); i++)
  {
	   cout << circuit2[i] + 1 << " ";
  }
  cin.get();
	*/

	return 0;
}
