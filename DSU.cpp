#include "DSU.h"

DSU::DSU(int n)
{
    p[n] = n;
    rank[n] = 0;
}

int DSU::find(int x)
{
    return (x == p[x] ? x : p[x] = find(p[x]));
}

bool DSU::unite(int x, int y)
{
    if((x = find(x)) == (y = find(y)))
    {
        return false;
    }

    if(rank[x] < rank[y])
    {
        p[x] = y;
    } else {
        p[y] = x;
    }
    
    if(rank[x] == rank[y])
    {
        ++rank[x];
    }
    return true;
}
